How to enable socket service on your computer? Just do this steps:
1) cp nzvb-node.service /usr/lib/systemd/system/nzvb-node.service
2) ln -s /usr/lib/systemd/system/nzvb-node.service /etc/systemd/system/multi-user.target.wants/nzvb-node.service
3) systemctl daemon-reload
4) systemctl enable nzvb-node.service
5) systemctl start nzvb-node.service
To read logs:
journalctl -u nzvb-node