var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var config = require('./config/main');
var mysql = require('mysql');
var pool = mysql.createPool(config.db);
var te_client = require('./te_client');
var Client = new te_client(config.calendar);

io.sockets.setMaxListeners(0);
Client.setMaxListeners(0);

Client.subscribe('calendar');
Client.on('message', function(msg){
  console.log('---------------');
  console.log(msg);
  var date = new Date();
  console.log(date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds());
  pool.getConnection(function(err, connection) {
    var sql = ' UPDATE wp_postmeta wp ' +
      ' INNER JOIN wp_postmeta wp1 ON wp1.post_id=wp.post_id ' +
      ' SET wp.meta_value=? ' +
      ' WHERE wp1.meta_key=? AND wp1.meta_value=? AND wp.meta_key=?';
    var params = [msg.actual, '_ecp_custom_8', msg.calendarId, '_ecp_custom_2'];
    connection.query(sql, params, function(err, result) {
      if (err) {
        console.log('Query error - ' + err);
      }
      connection.release();
    });
  });
});

io.on('connection', function (socket) {
  console.log('Socket connected');
  Client.on('message', function(msg){
    console.log('Message sent');
    socket.emit('calendarMessage', msg);
  });
});

server.listen(8890);